class AlbumController < ApplicationController
    # criando variável pra receber o que foi enviado e já foi verificado
    
    def index
        @album = Album.all
    end

    def show
    end

    
    def new
        @album = Album.new
    end
    
    def edit
    end
    
    def create 
        @album_novo = album_params
        puts params # onde a requisição fica encapsulada

        respond_to do |format|
        msg = { :status => "ok", :message => "Success!"}
        format.json  { render :json => msg }

        end
    end

    private 
    
    
    def set_album
        @album = Album.find(params[:id])
    end
    
    # Fazendo análise do que foi enviado e privando que apenas o que será guardado é o nome e a descrição
    def album_params
        params.require(:album).permit(:name, :description)
    end

end
