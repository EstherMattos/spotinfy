class CategoryController < ApplicationController
    # criando variável pra receber o que foi enviado e já foi verificado
    
    def index
        @category = Category.all
    end

    def show
    end

    
    def new
        @category = Category.new
    end
    
    def edit
    end
    
    def create 
        @category_novo = category_params
        puts params # onde a requisição fica encapsulada

        respond_to do |format|
        msg = { :status => "ok", :message => "Success!"}
        format.json  { render :json => msg }

        end
    end

    private 
    
    
    def set_category
        @category = Category.find(params[:id])
    end
    
    # Fazendo análise do que foi enviado e privando que apenas o que será guardado é o nome e a descrição
    def category_params
        params.require(:category).permit(:name)
    end

end
