class MainController < ApplicationController
    before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /people
  # GET /people.json
  def index
    @people = User.all
  end

  # GET /people/1
  # GET /people/1.json
  def show
  end

  # GET /people/new
  def new
    @user = User.new
    3.times { @user.potatoes.build } # Inicializa algumas batatas para o formulário.
  end

  # GET /people/1/edit
  def edit
    new_potatoes = 3 - @user.potatoes.count
    new_potatoes.times { @user.potatoes.build }
  end

  # POST /people
  # POST /people.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'user was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /people/1
  # PATCH/PUT /people/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'user was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /people/1
  # DELETE /people/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to people_url, notice: 'user was successfully destroyed.' }
      format.json { head :no_content }
      
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(
        :name, :age, :student, :birth, potatoes_attributes: [:id, :content, :happy, :_destroy]
      )
    end
end