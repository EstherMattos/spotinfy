class MusicController < ApplicationController
    # Gera ações
    # Pra receber o json sem token de autenticidade e manipular o bixano na braça
    before_action :set_music, only: [:show, :edit, :update, :destroy]

    skip_before_action :verify_authenticity_token

    def index
        @music = Music.all
    end
    
    def show
    end


    def new
        @music = Music.new
    end
          
    def edit
    end


    def create

    @music = Music.new(music_params)
    puts params # onde a requisição fica encapsulada
    
    respond_to do |format|
        msg = { :status => "ok", :message => "Success!"}
        format.json  { render :json => msg }

        if @music.save
            format.json { render :show, status: :created, location: @music }
        else
            format.json { render json: @music.errors, status: :unprocessable_entity }
          end

        end
    end
    
    
    private 

    def set_music
        @music = Music.find(params[:id])
    end

    def music_params  
        params.require(:music).permit(:name, :path, :compositor)
    end
end
