class PlaylistController < ApplicationController
    # criando variável pra receber o que foi enviado e já foi verificado
    
    def index
        @playlist = Playlist.all
    end

    def show
    end

    
    def new
        @playlist = Playlist.new
    end
    
    def edit
    end
    
    def create 
        @playlist_novo = playlist_params
        puts params # onde a requisição fica encapsulada

        respond_to do |format|
        msg = { :status => "ok", :message => "Success!"}
        format.json  { render :json => msg }

        end
    end

    private 
    
    
    def set_category
        @playlist = Playlist.find(params[:id])
    end
    
    # Fazendo análise do que foi enviado e privando que apenas o que será guardado é o nome e a descrição
    def playlist_params
        params.require(:playlist).permit(:name, :description)
    end

end
