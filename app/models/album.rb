class Album < ApplicationRecord
    
    # # Relação N:N
    has_many :users
    
    # # Relação 1:N
    has_many :musics

    # has_and_belongs_to_many :artists

end




# belongs_to (pertence a), 
# has_one (tem um), 
# has_many (tem muitos), 
# has_and_belongs_to_many (tem e pertence a muitos), 