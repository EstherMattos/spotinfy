class Music < ApplicationRecord
        #Relações com outros modelos

        # Relação 1:N
        belongs_to :albums   

        # # Relação N:N
        has_many :playlists

        # # Relação N:N
        has_many :users

        # # Relação N:N
        has_many :categories
      
        
end






# belongs_to (pertence a), 
# has_one (tem um), 
# has_many (tem muitos), 
# has_and_belongs_to_many (tem e pertence a muitos), 