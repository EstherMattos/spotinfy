class Playlist < ApplicationRecord

    # Relação N:N
    has_many :musics

    # #Relação N:N
    has_many :users

end






# belongs_to (pertence a), 
# has_one (tem um), 
# has_many (tem muitos), 
# has_and_belongs_to_many (tem e pertence a muitos), 