#Interação com os modelos
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  # # Relação de N:N
  has_and_belongs_to_many :musics

  # # Relação de N:N
  has_and_belongs_to_many :categoryies

  # # Relação de N:N
  has_and_belongs_to_many :playlists
end





# belongs_to (pertence a), 
# has_one (tem um), 
# has_many (tem muitos), 
# has_and_belongs_to_many (tem e pertence a muitos), 