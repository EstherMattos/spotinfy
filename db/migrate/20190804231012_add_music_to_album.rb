class AddMusicToAlbum < ActiveRecord::Migration[5.2]
  def change
    add_reference :albums, :music, foreign_key: true
  end
end
