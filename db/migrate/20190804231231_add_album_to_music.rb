class AddAlbumToMusic < ActiveRecord::Migration[5.2]
  def change
    add_reference :musics, :album, foreign_key: true
  end
end
