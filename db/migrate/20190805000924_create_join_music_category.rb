class CreateJoinMusicCategory < ActiveRecord::Migration[5.2]
  def change
    create_table :join_music_categories do |t|
      t.string :music
      t.string :category
    end
  end
end
