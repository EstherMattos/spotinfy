class CreateJoinMusicUser < ActiveRecord::Migration[5.2]
  def change
    create_table :join_music_users do |t|
      t.string :music
      t.string :user
    end
  end
end
