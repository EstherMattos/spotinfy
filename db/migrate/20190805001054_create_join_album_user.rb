class CreateJoinAlbumUser < ActiveRecord::Migration[5.2]
  def change
    create_table :join_album_users do |t|
      t.string :album
      t.string :user
    end
  end
end
